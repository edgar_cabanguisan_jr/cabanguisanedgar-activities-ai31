// author: Edgar M. Cabanguisan Jr.

const app = Vue.createApp({
    data() {
        return {
            title: "All Songs",
            allSongsDisplayed: true,
            selectedPlaylist: "",
            songs: [
                {
                    title: "All I Have To Give",
                    artist: "Backstreet Boys",
                    album: "Backstreet's Back",
                    duration: "4:02",
                    underPlaylist: ""
                },
                {
                    title: "Untouchable (Taylor's Version)",
                    artist: "Taylor Swift",
                    album: "Fearless (Taylor's Version)",
                    duration: "5:15",
                    underPlaylist: ""
                },
                {
                    title: "Dance",
                    artist: "Westlife",
                    album: "Spectrum",
                    duration: "2:45",
                    underPlaylist: ""
                }
            ],
            playlists: [
                "Playlist 1",
                "Playlist 2",
                "Playlist 3",
                "Playlist 4"
            ]
        }
    },

    methods: {
        songsInSelectedPlaylist(playlistName) {
            return this.songs.filter((s) => {
                return s.underPlaylist === playlistName
            })
        }
    },

    computed: {
        
    }
})

app.mount("#museec-app")
