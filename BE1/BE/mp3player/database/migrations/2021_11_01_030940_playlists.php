<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\QueryException;

class Playlists extends Migration {
    public function up() {
        Schema::create("playlists", function (Blueprint $table) {
            $table->increments("id");
            $table->string("name");
            $table->timestamp("created_at")->nullable();
            $table->timestamp("updated_at")->nullable();
        });
    }

    public function down() {
        Schema::dropIfExists("playlists");
    }
}
