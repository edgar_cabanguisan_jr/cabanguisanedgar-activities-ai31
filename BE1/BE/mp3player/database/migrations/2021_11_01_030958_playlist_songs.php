<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\QueryException;

class PlaylistSongs extends Migration {
    
    public function up() {
        Schema::create("playlist_songs", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("song_id");
            $table->integer("playlist_id");
            $table->timestamp("created_at")->nullable();
            $table->timestamp("updated_at")->nullable();
        });
    }

    public function down() {
        Schema::dropIfExists("playlist_songs");
    }
}
