<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\QueryException;

class Songs extends Migration {
    public function up() {
        Schema::create("songs", function (Blueprint $table) {
            $table->increments("id");
            $table->string("title");
            $table->integer("length");
            $table->string("artist");
            $table->timestamp("created_at")->nullable();
            $table->timestamp("updated_at")->nullable();
        });
    }

    
    public function down() {
        Schema::dropIfExists("songs");
    }
}
